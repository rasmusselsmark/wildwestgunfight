﻿using System;
using UnityEngine;

public static class Utils
{
    private static Vector2 _screenSize = Vector2.zero;

    public static Vector2 ScreenToGameObjectCoordinates (Vector3 screenPos)
    {
        Ray ray = Camera.main.ScreenPointToRay (screenPos);
        return ray.origin;
    }

    public static Vector2 GetScreenSize ()
    {
        if (_screenSize == Vector2.zero)
        {
            _screenSize = ScreenToGameObjectCoordinates (new Vector2 (Screen.width, Screen.height));
        }
        return _screenSize;
    }

    /// <summary>
    /// Calculate 2D sprite sort order to have player move in front of/behind objects
    /// Low (1) for large positive Y-coordinates (top of screen),
    /// high for large negative positive Y-coordinates (bottom of screen)
    /// </summary>
    /// <param name="y">The y coordinate of game object.</param>
    public static void SetSpriteSortOrder(SpriteRenderer spriteRenderer)
    {
        float y = spriteRenderer.transform.position.y; // - spriteRenderer.sprite.bounds.size.y / 2;
        spriteRenderer.sortingOrder = 500 - (int)((y + 10) * 10);
    }
}
