﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class GunfightScene : NetworkBehaviour
{
    public GameObject DecorationRocks;
    public GameObject DecorationPlants;

	void Start ()
    {
        if (isServer)
        {
            // server controls location of decoration
            DecorateScene (DecorationRocks, 7);
            DecorateScene (DecorationPlants, 10);
        }
	}

    private void DecorateScene(GameObject prefab, int count)
    {
        var screenSize = Utils.GetScreenSize ();

        for (int i = 0; i < count; i++)
        {
            var go = Instantiate(prefab);
            go.transform.position = new Vector2 (
                Random.Range (-screenSize.x + 0.5f, screenSize.x - 0.5f),
                Random.Range (screenSize.y - 0.5f, -screenSize.y + 0.5f));

            var decoration = go.GetComponent<Decoration> ();
            decoration.SpriteIndex = Random.Range (0, decoration.Sprites.Length);

            NetworkServer.Spawn(go);
        }
    }
}
