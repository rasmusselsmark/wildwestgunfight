﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Player : NetworkBehaviour
{
    public float Speed = 5.0f;
    public Bullet BulletPrefab;
    public Transform BulletSpawn;
    public GameObject Sprite;
    public UnityEngine.UI.Text HealthText;

    [SyncVar]
    public int Health = 3;

    void Start ()
    {
        UpdateHealthText ();
    }

	// Update is called once per frame
	void Update ()
    {
        if (!isLocalPlayer)
        {
            return;
        }

        var x = Input.GetAxis("Horizontal") * Time.deltaTime * Speed;
        var y = Input.GetAxis("Vertical") * Time.deltaTime * Speed;

        this.transform.Translate (x, y, 0);
        Utils.SetSpriteSortOrder (Sprite.GetComponent<SpriteRenderer> ());

        if (Input.GetKeyDown(KeyCode.Space))
        {
            CmdFire();
        }
	}

    [Command]
    void CmdFire()
    {
        var bullet = (Bullet)Instantiate (
            BulletPrefab,
            BulletSpawn.position,
            BulletSpawn.rotation);

        bullet.GetComponent<Rigidbody2D>().velocity = bullet.transform.right * bullet.Speed;

        NetworkServer.Spawn(bullet.gameObject);

        Destroy(bullet.gameObject, 2.0f);
    }

    public void GotHit ()
    {
        if (!isServer)
        {
            return;
        }

        Health--;
        UpdateHealthText ();

        if (Health <= 0)
        {
            Debug.Log (string.Format ("Player {0} died!", name));
        }
    }

    private void UpdateHealthText ()
    {
        string text = string.Format ("Lives {0}", Health);
        Debug.Log (text);

        if (HealthText != null)
        {
            HealthText.text = string.Format ("Lives {0}", Health);
        }
    }
}
