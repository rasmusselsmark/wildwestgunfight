﻿using UnityEngine;
using UnityEngine.Networking;

public class Decoration : NetworkBehaviour
{
    public Sprite[] Sprites;

    [SyncVar]
    public int SpriteIndex;

    void Start()
    {
        var sprite = GetComponent<SpriteRenderer> ();
        sprite.sprite = Sprites [SpriteIndex];
        Utils.SetSpriteSortOrder (sprite);
    }
}
